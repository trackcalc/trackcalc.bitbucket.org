(function ($) {
    'use strict';

    /*
     * Copyright © 2010, 2013 Tom Worster fsb@thefsb.org.
     * This program is free software: you can redistribute it and/or modify
     * it under the terms of the GNU General Public License as published by
     * the Free Software Foundation (http://www.gnu.org/licenses/gpl-2.0.html),
     * either version 2 of the License, or (at your option) any later version.
     */

    var
        rings,
        cogs,
        xscale = 40,
        xoff = -67.5,
        yscale = 40,
        yoff = 1.2,
        hsl2hex,
        morecst = false,
        moretcs = false;

    function make_gearchart() {
        var
            boxh = 20,
            mininch = Math.floor(27 * rings[0] / cogs[0] / 2) * 2,
            maxinch = Math.ceil(27 * rings[rings.length - 1] / cogs[cogs.length - 1] / 2) * 2,
            inches, top, left, width, c, r,
            html = [];
        for (inches = mininch; inches <= maxinch; inches += 2) {
            left = xscale * (xoff + inches);
            top = 0.2 * yscale;
            html.push('<div class="vline" style="left:' + left + 'px;"></div>');
            left -= 12.5;
            html.push('<p class="hscale top" style="left:' + left + 'px;">' + inches + '</p>');
            html.push('<p class="hscale bottom" style="left:' + left + 'px;">' + inches + '</p>');
        }
        for (c in cogs) {
            if (cogs.hasOwnProperty(c)) {
                top = yscale * (yoff + parseInt(c, 10));
                left = xscale * (xoff + 27 * rings[0] / cogs[c]);
                width =
                    xscale * (xoff + 27 * rings[rings.length - 1] / cogs[c]) -
                        xscale * (xoff + 27 * rings[0] / cogs[c]);
                html.push('<div id="cog' + cogs[c] + '" class="line" style="top:' + top + 'px; left:' + left +
                    'px; width:' + width + 'px;"></div>');
                top -= boxh / 2;
                for (r in rings) {
                    if (rings.hasOwnProperty(r)) {
                        inches = 27 * rings[r] / cogs[c];
                        left = xscale * (xoff + inches) - boxh / 2;
                        html.push('<p id="f' + rings[r] + 'r' + cogs[c] + '" class="box" style="top:' + top +
                            'px; left:' +
                            left + 'px;">' + rings[r] + '</p>');
                    }
                }
            }
        }
        $(html.join('')).appendTo('#gearplane');
        html = [];
        for (c in cogs) {
            if (cogs.hasOwnProperty(c)) {
                top = yscale * (parseInt(c, 10) + 1.2) - 8;
                html.push('<p style="top:' + top + 'px">' + cogs[c] + '</p>');
            }
        }
        $(html.join('')).appendTo('#gearsel div.vscale');
    }

    /*
     * The following functions share these parameters:
     * @param f int     # teeth on front cog
     * @param r int     # teeth on rear cog
     * @param n int     # links in chain
     * @param p number  Chain pitch
     * @param c number  c-to-c BB to rear hub distance
     */

    function chain1(f, r, n, p) {
        var sqrt = Math.sqrt, pow = Math.pow, PI = Math.PI;
        return (p * sqrt(pow(n + (-f - r) / 2.0, 2) - pow(f - r, 2) / pow(PI, 2))) / 2.0;
    }

    function nlinks1(f, r, c, p) {
        // only approximate
        var sqrt = Math.sqrt, pow = Math.pow, PI = Math.PI;
        return (f + r +
            (2 * sqrt(pow(f, 2) * pow(p, 4) + 4 * pow(c, 2) * pow(p, 2) * pow(PI, 2)
                - 2 * f * pow(p, 4) * r + pow(p, 4) * pow(r, 2))) / (pow(p, 2) * PI)) / 2.0;
    }

    function fn(c, f, r, n, p) {
        var pow = Math.pow, PI = Math.PI, asin = Math.asin;
        return pow(c, 2) - (pow(p, 2) * pow(f - r, 2)) / (4.0 * pow(PI, 2)) -
            (pow(p, 2) * pow(PI * (f - 2 * n + r) + 2 * (f - r) * asin((p * (f - r)) / (2.0 * c * PI)), 2)) /
                (16.0 * pow(PI, 2));
    }

    function dFdc(c, f, r, n, p) {
        var sqrt = Math.sqrt, pow = Math.pow, PI = Math.PI, asin = Math.asin;
        return 2 * c + (pow(p, 3) * pow(f - r, 2) *
            (PI * (f - 2 * n + r) + 2 * (f - r) * asin((p * (f - r)) / (2.0 * c * PI)))) /
            (4.0 * pow(c, 2) * pow(PI, 2) * sqrt(-((pow(f, 2) * pow(p, 2) - 4 * pow(c, 2) * pow(PI, 2) -
                2 * f * pow(p, 2) * r + pow(p, 2) * pow(r, 2)) / pow(c, 2))));
    }

    function chain2(f, r, n, p) {
        var cn = chain1(f, r, n, p),
            cn1,
            err,
            tol = Math.pow(10, -6),
            i = 0;
        do {
            cn1 = cn - fn(cn, f, r, n, p) / dFdc(cn, f, r, n, p);
            err = Math.abs((cn - cn1) / cn);
            i += 1;
            cn = cn1;
        } while (err > tol && i < 15);
        return cn;
    }

    function chainTable2(ccmin, ccmax, p) {
        var cc, nlo, nhi, n, t = {};
        if (p === undefined) {
            p = 12.7;
        }
        rings.forEach(function (f) {
            cogs.forEach(function (r) {
                nlo = Math.floor(nlinks1(f, r, ccmin, p) / 2) * 2 - 4;
                nhi = Math.ceil(nlinks1(f, r, ccmax, p) / 2) * 2 + 4;
                for (n = nlo; n <= nhi; n += 2) {
                    cc = chain2(f, r, n, p);
                    if (ccmin <= cc && cc <= ccmax) {
                        if (!t[f]) {
                            t[f] = {};
                        }
                        if (!t[f][r]) {
                            t[f][r] = {};
                        }
                        t[f][r][n] = cc;
                    }
                }
            });
        });
        return t;
    }

    function hsl2rgb(h, s, l) {
        var c, x, m, rgb;
        if (typeof h !== 'number') {
            return [0, 0, 0];
        }
        h = h % 360;
        if (h < 0) {
            h += 360;
        }
        s = typeof s === 'number' ? Math.min(Math.max(s, 0), 1) : 1;
        l = typeof l === 'number' ? Math.min(Math.max(l, 0), 1) : 0.5;
        c = s * (1 - Math.abs(2 * l - 1));
        x = c * (1 - Math.abs((h / 60) % 2 - 1));
        rgb =
            h < 60 ? [c, x, 0] : (
                h < 120 ? [x, c, 0] : (
                    h < 180 ? [0, c, x] : (
                        h < 240 ? [0, x, c] : (
                            h < 300 ? [x, 0, c] : (
                                [c, 0, x]
                            )
                        )
                    )
                )
            );
        m = l - c / 2;
        rgb[0] += m;
        rgb[1] += m;
        rgb[2] += m;
        return rgb;
    }

    String.prototype.lpad = function (padString, length) {
        var str = this;
        while (str.length < length) {
            str = padString + str;
        }
        return str;
    };

    hsl2hex = function (h, s, l) {
        var rgb = hsl2rgb(h, s, l);
        return '#'
            + Math.floor(rgb[0] * 255).toString(16).lpad('0', 2)
            + Math.floor(rgb[1] * 255).toString(16).lpad('0', 2)
            + Math.floor(rgb[2] * 255).toString(16).lpad('0', 2);
    };

    function contrast_palette(ncols, s, l) {
        var off, i, palette = [];
        if (ncols === 4) {
            ncols = 5;
        }
        off = Math.floor((ncols + 1) / 2);
        for (i = 0; i < ncols; i += 1) {
            palette.push(hsl2hex((Math.floor(i / 2) + (i % 2 ? off : 0)) / ncols * 360, s, l));
        }
        return palette;
    }

    function htmlChainTable2(ccmin, ccmax, p) {
        var n, pal, grange, gmin, nmax, nmin,
            t = chainTable2(ccmin, ccmax, p),
            html = [];

        // default chain pitch is half an inch 12.7mm
        if (p === undefined) {
            p = 12.7;
        }

        // gear ratio color scale range and minimum
        grange = rings[rings.length - 1] / cogs[cogs.length - 1] - rings[0] / cogs[0];
        gmin = rings[0] / cogs[0];

        // find the smallest and largest n (num. chain links) values
        nmin = Number.MAX_VALUE;
        nmax = 0;
        rings.forEach(function (f) {
            cogs.forEach(function (r) {
                var n;
                if (t[f][r]) {
                    for (n in t[f][r]) {
                        if (t[f][r].hasOwnProperty(n)) {
                            n = parseInt(n, 10);
                            if (n < nmin) {
                                nmin = n;
                            }
                            if (n > nmax) {
                                nmax = n;
                            }
                        }
                    }
                }
            });
        });
        pal = contrast_palette((nmax - nmin) / 2 + 1, 0.85, 0.75);

        html.push('<table>');
        html.push('<tr><th><th colspan="' + (cogs.length + 1) + '">Cog');
        html.push('<tr><th>Ring');
        cogs.forEach(function (r) {
            html.push('<th>' + r);
        });
        rings.forEach(function (f) {
            html.push('<tr><td class="f">' + f);
            cogs.forEach(function (r) {
                var n;
                html.push(
                    '<td class="v" style="background-color:' +
                        hsl2hex(0, 0, 0.5 + 0.4 * (f / r - gmin) / grange) + '">'
                );
                html.push('<p class="n">');
                if (t[f][r]) {
                    for (n in t[f][r]) {
                        if (t[f][r].hasOwnProperty(n)) {
                            html.push(
                                '<span class="n n' + n + '" style="background-color:' +
                                    pal[Math.floor((n - nmin) / 2)] + '"> ' /*+ n + ': ' +
                                    t[f][r][n].toPrecision(3)*/ + '</span>'
                            );
                        }
                    }
                }
                html.push(
                    '<div class="s"><div style="width:' + Math.round((f / r - gmin) / grange * 97) +
                        '%"></div></div>'
                );
                html.push(
                    '<p class="i i' + Math.floor(27 * f / r) + '">' + (27 * f / r).toPrecision(3)
                );
            });
        });
        html.push('<tr><td class="v" colspan="' + (cogs.length + 1) + '"><p class="key">#links: ');
        for (n = nmin; n <= nmax; n += 2) {
            html.push(
                '<span class="n n' + n + '" style="background-color:' +
                    pal[Math.floor((n - nmin) / 2)] + '"> ' + n + ' </span> '
            );
        }
        html.push('</table>');
        return html;
    }

    function spacings() {
        var f = parseInt($('#f').val(), 10),
            r = parseInt($('#r').val(), 10),
            wdiam = parseFloat($('#wcin').val()) / Math.PI,
            p = parseFloat($('#pin').val()),
            n = Math.floor(nlinks1(f, r, wdiam * 0.55, p) / 2) * 2,
            nmax = Math.ceil(nlinks1(f, r, (wdiam * 0.65), p)),
            html = [],
            s;
        for (1; n <= nmax; n += 2) {
            s = chain2(f, r, n, p);
            html.push(
                '<tr><td>' + n +
                    '</td><td class="k">' + (25.4 * s).toPrecision(3) +
                    '</td><td class="m">' + s.toPrecision(3) + '</td></tr>'
            );
        }
        return html.join('');
    }

    function speeds() {
        var f = parseInt($('#f').val(), 10), // front cog teeth
            r = parseInt($('#r').val(), 10), // rear cog teeth
            wc = parseFloat($('#wcmm').val()) / 1000, // wheel circumference in meters
            roll = wc * f / r, // rollout in meters
            rpm,
            s, // speed in m/s
            l,
            laps = [200, 250, 333], // lap length in mters
            t = 0, // lap time and lap time increment
            i = 0,
            j,
            html = [],
            rowsperhead = 15,
            inc = morecst ? 1 : 5,
            $cst = $('#cst'),
            $tcs = $('#tcs'),
            head = '<tbody class="head">' + $cst.find('tbody.head').html() + '</tbody>';
        html.push(head);
        for (rpm = 90; rpm <= 160; rpm += inc) {
            if (i === 0 || (morecst && i % rowsperhead === 0)) {
                html.push('<tbody class="data">');
            }
            s = rpm / 60 * roll;
            html.push(
                '<tr><td class="c">' + rpm +
                    '</td><td class="k">' + (3.6 * s).toPrecision(3) +
                    '</td><td class="m">' + (2.23693629 * s).toPrecision(3) +
                    '</td><td class="t">' + (200 / s).toPrecision(3) +
                    '</td><td class="t">' + (250 / s).toPrecision(3) +
                    '</td><td class="t">' + (333.33 / s).toPrecision(3) +
                    '</td><td class="t">' + (500 / s).toPrecision(3) +
                    '</td><td class="t">' + (750 / s).toPrecision(3) +
                    '</td><td class="t">' + (1000 / s).toPrecision(3) +
                    '</td></tr>'
            );
            i += 1;
            // WARNING: == or === ?
            if (morecst && i % rowsperhead === 0 && rpm + inc < 160) {
                html.push('</tbody>');
                html.push(head);
            }
        }
        html.push('</tbody>');
        $cst.empty().html((html.join('\n')));

        rowsperhead = 30;
        inc = moretcs ? 0.1 : 0.5;
        html = [];
        head = '<tbody class="head">' + $tcs.find('tbody.head').html() + '</tbody>';
        for (i = 0; true; i += 1) {
            if (i === 0 || (moretcs && i % rowsperhead === 0)) {
                if (t > 0) {
                    html.push('</tbody>');
                }
                html.push(head);
                html.push('<tbody class="data">');
            }
            html.push('<tr>');
            for (j = 0; j < laps.length; j += 1) {
                l = laps[j]; // lap distance in meters
                t = Math.floor(l / (160 / 60 * roll) / inc) * inc + inc * i; // lap time in seconds
                s = l / t; // speed in m/s
                html.push('<td class="t">' + t.toPrecision(3) + '</td>');
                if (l === 250) {
                    html.push(
                        '<td class="t">' + (2 * t).toPrecision(3) +
                            '</td><td class="t">' + (3 * t).toPrecision(3) +
                            '</td><td class="t">' + (4 * t).toPrecision(3) + '</td>'
                    );
                }
                html.push(
                    '<td class="c">' + (60 * s / roll).toPrecision(3) +
                        '</td><td class="k">' + (s * 3.6).toPrecision(3) +
                        '</td><td class="m">' + (s * 2.23693629).toPrecision(3) + '</td>'
                );
            }
            html.push('</tr>');
            if (60 * s / roll < 90) {
                break;
            }
        }
        html.push('</tbody>');
        $tcs.empty().html((html.join('\n')));
    }

    function do_revs() {
        var f = parseInt($('#f').val(), 10),
            r = parseInt($('#r').val(), 10),
            wcmm = parseFloat($('#wcmm').val()),
            laps = [200, 250, 333, 400, 500, 750, 1000],
            html1 = [],
            html2 = [],
            l;
        for (l in laps) {
            if (laps.hasOwnProperty(l)) {
                html1.push('<td>' + (laps[l]) + '</td>');
                html2.push('<td class="c">' + (1000 * laps[l] / wcmm * r / f).toPrecision(3) + '</td>');
            }
        }
        $('#distances td, #crankrevs td').remove();
        $(html1.join('')).appendTo('#distances');
        $(html2.join('')).appendTo('#crankrevs');
    }

    function calc() {
        var f = parseInt($('#f').val(), 10),
            r = parseInt($('#r').val(), 10),
            wcin = parseFloat($('#wcin').val()),
            wcmm = parseFloat($('#wcmm').val()),
            mph160 = wcin * f / r * 160 * 60 / 63360,
            inches = 27 * f / r,
            left = xscale * (xoff + inches) - 375;
        $('#gearplane').animate({scrollLeft: left}, 1000);
        $('#inches').text(inches.toPrecision(4));
        $('#inches2').text((wcin / Math.PI * f / r).toPrecision(4));
        $('#wcin2').text(wcin.toPrecision(4));
        $('span.ring').text(f);
        $('span.cog').text(r);
        $('#devin').text((wcin * f / r).toPrecision(4));
        $('#devm').text((wcmm * f / r / 1000).toPrecision(4));
        speeds();
        $('#spacing').empty().html(spacings());
        do_revs();

        $.plot(
            $("#flot"),
            [
                {data: [
                    [0, 0],
                    [160, mph160]
                ]},
                {data: [
                    [160, 1.609344 * mph160]
                ], yaxis: 2}
            ],
            {
                xaxis: { min: 60, max: 160 },
                yaxis: { max: Math.ceil(mph160 / 5) * 5 },
                y2axis: { min: 0, max: 1.609344 * mph160 }
            }
        );
    }

    function circum_select(e) {
        var $elem = $(e.target),
            x = parseFloat($elem.val());
        if (typeof x === 'number' && !isNaN(x) && x > 0) {
            $('#wcin').val((Math.PI * x).toPrecision(4));
            $('#wcmm').val((Math.PI * x * 25.4).toPrecision(4));
        }
        calc();
        return true;
    }

    function do_dim(e) {
        var id, x, $wdsel, $elem;

        $elem = e instanceof $ && e.length ? e : $(e.target);
        id = $elem.attr('id');

        if (id) {
            id = id.match(/^([a-z]+)(in|mm)$/);
        }
        if (id) {
            x = parseFloat($elem.val());
            if (typeof x === 'number' && !isNaN(x) && x > 0) {
                if (id[2] === 'mm') {
                    $('#' + id[1] + 'in').val((x / 25.4).toPrecision(4));
                } else {
                    $('#' + id[1] + 'mm').val((x * 25.4).toPrecision(4));
                }
                if (id[1] === 'wc') {
                    x = ($('#wcin').val() / Math.PI).toPrecision(4);
                    $wdsel = $('#wdsel');
                    $wdsel.val(x);
                    if (!$wdsel.val()) {
                        $wdsel.val('user');
                    }
                }
            }
        }
        if (e.type === 'change') {
            calc();
        }
        return true;
    }

    function box_click(e) {
        var $box = $(e.target), ratio;
        $('#gearplane').find('p.box').removeClass('selected');
        $box.addClass('selected');
        ratio = $box.attr('id');
        if (ratio) {
            ratio = ratio.match(/^f(\d+)r(\d+)$/);
        }
        if (ratio) {
            $('#f').val(ratio[1]);
            $('#r').val(ratio[2]);
            calc();
        }
    }

    function pick_box() {
        $('#gearplane').find('p.box').removeClass('selected');
        $('#f' + $('#f').val() + 'r' + $('#r').val()).addClass('selected');
        calc();
    }

    function morefewer(e) {
        var $elem = $(e.target),
            $parent = $elem.parent();
        e.preventDefault();
        if ($elem.text() === 'more') {
            $('span', $parent).replaceWith('<a href="#" class="fewer">fewer</a>').click(morefewer);
            $elem.replaceWith('<span class="more">more</span>');
            if ($parent.hasClass('cst')) {
                morecst = true;
            } else {
                moretcs = true;
            }
        } else {
            $('span', $parent).replaceWith('<a href="#" class="more">more</a>').click(morefewer);
            $elem.replaceWith('<span class="fewer">fewer</span>');
            if ($parent.hasClass('cst')) {
                morecst = false;
            } else {
                moretcs = false;
            }
        }
        $('a', $parent).click(morefewer);
        speeds();
    }

    $.fn.setup_calculator = function () {
        var $f, $cstblock;
        $('#selflink').attr('href', location.href);
        make_gearchart();
        $f = $('#f');
        $f.val('48');
        $('#r').val('15');
        $('#params').find('input.dim').bind('keyup change', do_dim);
        $('#wcin').val('82.56').keyup();
        $('#pin').val('0.5').keyup();
        $('#wdsel').bind('change', circum_select);
        $('#gearplane').find('p.box').click(box_click);
        $('#f, #r').change(pick_box);
        $('#go').click(calc);
        $f.change();
        $('span.nrows a').click(morefewer);
        $cstblock = $('#cstblock');
        $cstblock.css({'width': $('#cst').width() + 20, 'height': $cstblock.height() + 20});
        $cstblock.css({'width': $('#tcs').width() + 20, 'height': $cstblock.height() + 20});
    };

    $.fn.make_my_table = function (ccmin, ccmax, options) {
        var opts = $.extend({}, $.fn.make_my_table.defaults, options);
        rings = opts.rings;
        cogs = opts.cogs;
        $(this).append(htmlChainTable2(ccmin, ccmax, opts.p).join(''));
    };

    $.fn.make_my_table.defaults = {
        rings: [44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 53, 54, 55],
        cogs: [17, 16, 15, 14, 13],
        p: 12.7,
        d: 27
    };

    rings = $.fn.make_my_table.defaults.rings;
    cogs = $.fn.make_my_table.defaults.cogs;
}(jQuery));
